# kiloupabocou

clone the project
cd kiloupabocou

cp .env.tpl .env

fill your environment variables in the .env

dont forget to modify DATABASE_URL line 29 in the .env in symfony folder

and run:

docker-compose up -d --build

and you can access to your bdd at localhost:8080
and your app at localhost:8001

to enter any container with bash :

docker exec -it  container_name bash


